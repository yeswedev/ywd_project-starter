laravel_initialize_project_on_server() {
	printf 'Initialisation du projet Laravel…\n'
	check_dependencies 'php' 'composer' 'npm'
	check_variables 'PROJECT_NAME' 'SERVER_USER'
	if [ -z "$PROJECT_PATH" ]; then
		PROJECT_PATH="/home/$SERVER_USER/$PROJECT_NAME.git"
	fi
	if ! [ -x "$PROJECT_PATH/artisan" ]; then
		chmod 775 "$PROJECT_PATH"/artisan
	fi
	if [ "$DEBUG" = 'true' ] && [ "$DEBUG_GENERIC" = 'true' ]; then
		su --login --command "cd $PROJECT_PATH && cp .env.example .env && composer install && ./artisan key:generate && npm install && npm run dev" "$SERVER_USER"
	else
		su --login --command "cd $PROJECT_PATH && cp .env.example .env && composer install && ./artisan key:generate && npm install && npm run dev" "$SERVER_USER" >/dev/null 2>&1
	fi
	chmod 664 "$PROJECT_PATH"/artisan
	chgrp --recursive 'www-data' "$PROJECT_PATH/bootstrap" "$PROJECT_PATH/storage"
	chmod --recursive ug+rwX "$PROJECT_PATH/bootstrap" "$PROJECT_PATH/storage"
	printf '\033[1;32mOK\033[0m\n'
}
laravel_database_link() {
	check_variables 'SERVER_USER' 'PROJECT_NAME' 'DB_HOST' 'DB_NAME' 'DB_USER' 'DB_PASS'
	check_dependencies 'sed'
	file="/home/$SERVER_USER/$PROJECT_NAME.git/.env"
	check_files "$file"
	pattern="s#^\\(DB_HOST\\)=.*#\\1='$DB_HOST'#"
	pattern="$pattern;s#^\\(DB_DATABASE\\)=.*#\\1='$DB_NAME'#"
	pattern="$pattern;s#^\\(DB_USERNAME\\)=.*#\\1='$DB_USER'#"
	pattern="$pattern;s#^\\(DB_PASSWORD\\)=.*#\\1='$DB_PASS'#"
	sed --in-place "$pattern" "$file"
	return 0
}
laravel_initiate_project() {
	# shellcheck disable=SC1112
	printf 'Démarrage d’un projet Laravel, cette étape peut prendre plusieurs minutes…\n'
	check_dependencies 'composer' 'git' 'sed'
	composer create-project --prefer-dist laravel/laravel "$LOCAL_REPO_PATH" >/dev/null 2>&1
	(
		# shellcheck disable=SC2164
		cd "$LOCAL_REPO_PATH"
		git init >/dev/null
		git add .
		git commit --message='Fresh Laravel installation' >/dev/null
		git rm -r 'public/favicon.ico' 'public/robots.txt' 'public/web.config' >/dev/null
		cat > 'public/.gitignore' <<- 'EOF'
		*
		!.gitignore
		!index.php
		EOF
		git add 'public/.gitignore'
		git commit --message='Do not track files in /public' >/dev/null
		file='app/Providers/AppServiceProvider.php'
		pattern='s#^use Illuminate\\Support\\ServiceProvider;$#&\nuse Illuminate\\Support\\Facades\\Schema;#'
		pattern="$pattern"';s#//#Schema::defaultStringLength(191);#'
		sed --in-place "$pattern" "$file"
		git add "$file"
		git commit --message='Fix index length issue with some versions of MariaDB' >/dev/null
		# Include GrumPHP for automatic tests
		npm install >/dev/null 2>&1
		cat > 'grumphp.yml' <<- 'EOF'
		parameters:
		    git_dir: .
		    bin_dir: vendor/bin
		    tasks:
		        composer: ~
		        composer_require_checker: ~
		        git_blacklist:
		            keywords:
		                - "^\\s*dd("
		                - "die("
		                - "dump("
		                - "exit;"
		                - "var_dump("
		                - "@elsif("
		                - "@elsif ("
		        npm_script:
		            script: "production"
		            triggered_by:
		                - "js"
		                - "scss"
		            is_run_task: true
		        phpcpd:
		            exclude:
		                - "database/migrations"
		                - "resources/lang"
		                - "storage"
		                - "vendor"
		        phplint:
		            ignore_patterns:
		                - "_ide_helper.php"
		        securitychecker: ~
		EOF
		composer require --dev phpro/grumphp maglnet/composer-require-checker sebastian/phpcpd jakub-onderka/php-parallel-lint sensiolabs/security-checker >/dev/null 2>&1
		git add 'composer.json' 'composer.lock' 'grumphp.yml'
		git commit --message='Include GrumPHP for automatic tests' >/dev/null 2>&1
	)
	printf '\033[1;32mOK\033[0m\n'
}

