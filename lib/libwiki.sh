wiki_write_page() {
	check_variables 'WIKI_ENGINE'
	case "$WIKI_ENGINE" in
		('dokuwiki')
			dokuwiki_write_page
		;;
		('none')
			true
		;;
		(*)
			wiki_unknown_engine
		;;
	esac
}
wiki_send_page() {
	check_variables 'WIKI_ENGINE'
	case "$WIKI_ENGINE" in
		('dokuwiki')
			dokuwiki_send_page
		;;
		('none')
			true
		;;
		(*)
			wiki_unknown_engine
		;;
	esac
}
wiki_display_url() {
	check_variables 'WIKI_ENGINE'
	case "$WIKI_ENGINE" in
		('dokuwiki')
			dokuwiki_display_url
		;;
		('none')
			true
		;;
		(*)
			wiki_unknown_engine
		;;
	esac
}
wiki_unknown_engine() {
	# shellcheck disable=SC1112
	printf 'Erreur : le moteur "%s" n’est pas géré.\n' "$WIKI_ENGINE"
	return 1
}

