git_set_repository_host() {
	check_variables 'GIT_ENGINE'
	case "$GIT_ENGINE" in
		('gitlab')
			# Nothing to do here
			true
		;;
		(*)
			git_error_unkown_host
		;;
	esac
}
git_create_repository() {
	check_variables 'GIT_ENGINE'
	case "$GIT_ENGINE" in
		('gitlab')
			gitlab_create_repository
		;;
		(*)
			git_error_unkown_host
		;;
	esac
	git_get_repository_url
	git_get_repository_http_url
	check_variables 'GIT_REPO_URL' 'GIT_REPO_HTTP_URL'
	cat >> "$SCRIPT_PATH/config/project.info" <<- EOF
	GIT_REPO_URL='$GIT_REPO_URL'
	GIT_REPO_HTTP_URL='$GIT_REPO_HTTP_URL'
	EOF
}
git_get_repository_name() {
	check_variables 'PROJECT_TECH' 'PROJECT_NAME'
	GIT_REPO_NAME="$(printf '%s' "${PROJECT_TECH}_$PROJECT_NAME" | sed 's/^wordpress_vanilla_/wp_/;s/^wordpress_/wp_/')"
	export GIT_REPO_NAME
}
git_get_repository_http_url() {
	check_variables 'GIT_ENGINE'
	case "$GIT_ENGINE" in
		('gitlab')
			# Nothing to do here
			true
		;;
		(*)
			git_error_unkown_host
		;;
	esac
}
git_get_local_repository_path() {
	if [ -z "$LOCAL_REPO_PATH" ]; then
		check_dependencies 'mktemp'
		check_variables 'PROJECT_NAME'
		LOCAL_REPO_PATH=$(mktemp --directory --tmpdir="${TMPDIR:-/tmp}" "$PROJECT_NAME.XXXXX")
	fi
	export LOCAL_REPO_PATH
}
git_link_to_distant_repository() {
	check_dependencies 'git'
	check_variables 'GIT_ENGINE' 'LOCAL_REPO_PATH'
	case "$GIT_ENGINE" in
		('gitlab')
			# Nothing to do here
			true
		;;
		(*)
			git_error_unkown_host
		;;
	esac
	printf 'Mise à jour du dépôt git distant…\n'
	(
		cd "$LOCAL_REPO_PATH" || return 1
		git remote add origin "$GIT_REPO_URL"
		git push --set-upstream origin master
		git checkout -b staging master
		git push --set-upstream origin staging
		git checkout -b dev staging
		git push --set-upstream origin dev
	) >/dev/null 2>/dev/null
	printf '\033[1;32mOK\033[0m\n'
}
git_add_branch_restrictions() {
	check_variables 'GIT_ENGINE'
	case "$GIT_ENGINE" in
		('gitlab')
			gitlab_add_branch_restrictions
		;;
		(*)
			git_error_unkown_host
		;;
	esac
}
git_get_branch_restrictions() {
	check_variables 'GIT_REPO_HOST'
	case "$GIT_REPO_HOST" in
		(*)
			git_error_unkown_host
		;;
	esac
}
git_initiate_project() {
	check_dependencies 'git'
	check_variables 'PROJECT_TECH' 'LOCAL_REPO_PATH'
	case "$PROJECT_TECH" in
		('laravel')
			laravel_initiate_project
		;;
		('wordpress'*)
			wordpress_initiate_project
		;;
		('prestashop'*)
			prestashop_initiate_project
		;;
		('drupal')
			drupal_initiate_project
		;;
		('custom')
			(
				cd "$LOCAL_REPO_PATH" || return 1
				git init >/dev/null
			)
		;;
	esac
	printf 'Inclusion de lʼoutil de déploiement Yes We Dev au projet…\n'
	(
		cd "$LOCAL_REPO_PATH" || return 1
		if [ "$DEBUG" = 'true' ] && [ "$DEBUG_GIT" = 'true' ]; then
			git submodule add 'https://framagit.org/yeswedev/ywd_deploy.git' 'ywd_deploy'
			git commit -m 'Include YWD Toolbox'
		else
			git submodule add 'https://framagit.org/yeswedev/ywd_deploy.git' 'ywd_deploy' >/dev/null 2>&1
			git commit -m 'Include YWD Toolbox' >/dev/null 2>&1
		fi
	)
	printf '\033[1;32mOK\033[0m\n'
}
git_get_repository_url() {
	check_variables 'GIT_ENGINE'
	case "$GIT_ENGINE" in
		('gitlab')
			# Nothing to do here
			true
		;;
		(*)
			git_error_unkown_host
		;;
	esac
}
git_add_ssh_deploy_key() {
	check_variables 'GIT_ENGINE'
	case "$GIT_ENGINE" in
		('gitlab')
			gitlab_add_ssh_deploy_key
		;;
		(*)
			git_error_unkown_host
		;;
	esac
}
git_error_unkown_host() {
	# shellcheck disable=SC1112
	printf 'Erreur : l’hôte %s n’est pas géré.\n' "$GIT_REPO_HOST"
	return 1
}
git_clone_repository_on_server() {
	printf 'Clonage du dépôt git sur le serveur…\n'
	check_dependencies 'ssh-keyscan' 'git'
	check_variables 'PROJECT_HOSTNAME' 'PROJECT_NAME' 'SERVER_USER' 'GIT_ENGINE'
	git_get_repository_url
	check_variables 'GIT_REPO_URL'
	PROJECT_PATH="/srv/http/$PROJECT_HOSTNAME"
	rm --recursive --force "$PROJECT_PATH"
	case "$GIT_ENGINE" in
		('gitlab')
			# shellcheck disable=SC2039
			local gitlab_hostname
			check_variables 'GITLAB_BASE_URL'
			gitlab_hostname=$(printf '%s' "$GITLAB_BASE_URL" | cut --delimiter='/' --fields=3)
			if [ "$DEBUG" = 'true' ] && [ "$DEBUG_SSH" = 'true' ]; then
				su --login --command "ssh-keyscan -v -t rsa $gitlab_hostname >> ~/.ssh/known_hosts" "$SERVER_USER"
			else
				su --login --command "ssh-keyscan -t rsa $gitlab_hostname >> ~/.ssh/known_hosts" "$SERVER_USER" >/dev/null 2>&1
			fi
		;;
		(*)
			echo "Erreur : la forge git $GIT_ENGINE n’est pas gérée."
			exit 1
		;;
	esac
	if [ "$DEBUG" = 'true' ] && [ "$DEBUG_GIT" = 'true' ]; then
		echo "git clone --branch dev $GIT_REPO_URL $PROJECT_NAME.git"
		su --login --command "git clone --branch dev $GIT_REPO_URL $PROJECT_NAME.git" "$SERVER_USER"
	else
		su --login --command "git clone --branch dev $GIT_REPO_URL $PROJECT_NAME.git" "$SERVER_USER" >/dev/null 2>&1
	fi
	ln --symbolic "/home/$SERVER_USER/$PROJECT_NAME.git" "$PROJECT_PATH"
	printf '\033[1;32mOK\033[0m\n'
	check_variables 'PROJECT_TECH'
	case "$PROJECT_TECH" in
		('laravel')
			laravel_initialize_project_on_server
		;;
		('wordpress'*)
			wordpress_initialize_project_on_server
		;;
		('prestashop'*)
			prestashop_initialize_project_on_server
		;;
		('drupal')
			drupal_initialize_project_on_server
		;;
		(*)
			true
		;;
	esac
}
