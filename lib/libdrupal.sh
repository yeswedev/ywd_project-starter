drupal_initialize_project_on_server() {
	printf 'Initialisation du projet Drupal…\n'
	check_dependencies 'composer'
	check_variables 'PROJECT_NAME' 'SERVER_USER'
	if [ -z "$PROJECT_PATH" ]; then
		PROJECT_PATH="/home/$SERVER_USER/$PROJECT_NAME.git"
	fi
	if [ "$DEBUG" = 'true' ] && [ "$DEBUG_GENERIC" = 'true' ]; then
		su --login --command "cd $PROJECT_PATH && composer install" "$SERVER_USER"
	else
		su --login --command "cd $PROJECT_PATH && composer install" "$SERVER_USER" >/dev/null 2>&1
	fi
	directory="$PROJECT_PATH/config"
	mkdir "$directory"
	# shellcheck disable=SC2086
	chown $SERVER_USER.www-data "$directory"
	printf '\033[1;32mOK\033[0m\n'
}
drupal_database_link() {
	# database link for Drupal project is done through the interactive install
	return 0
}
drupal_initiate_project() {
	# shellcheck disable=SC1112
	printf 'Démarrage d’un projet Drupal, cette étape peut prendre plusieurs minutes…\n'
	check_dependencies 'composer'
	composer create-project --no-interaction --stability dev drupal-composer/drupal-project:8.x-dev "$LOCAL_REPO_PATH" >/dev/null 2>&1
	(
		# shellcheck disable=SC2164
		cd "$LOCAL_REPO_PATH"
		git init >/dev/null
		git add .
		git commit -m 'Fresh Drupal installation' >/dev/null
	)
	printf '\033[1;32mOK\033[0m\n'
}

