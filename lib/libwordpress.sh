wordpress_initialize_project_on_server() {
	check_variables 'PROJECT_TECH' 'PROJECT_NAME' 'SERVER_USER' 'PROJECT_HOSTNAME'
	if [ -z "$PROJECT_PATH" ]; then
		PROJECT_PATH="/home/$SERVER_USER/$PROJECT_NAME.git"
	fi
	case "$PROJECT_TECH" in
		('wordpress_vanilla')
			wordpress_initialize_project_on_server_vanilla
			;;
		(*)
			wordpress_initialize_project_on_server_bedrock
			;;
	esac
	chgrp --recursive "www-data" "$PROJECT_PATH"
	chmod --recursive g+rw "$PROJECT_PATH"
	printf '\033[1;32mOK\033[0m\n'
	return 0
}
wordpress_initialize_project_on_server_bedrock() {
	printf 'Initialisation du projet WordPress + Bedrock…\n'
	check_dependencies 'composer'
	if [ "$DEBUG" = 'true' ] && [ "$DEBUG_SSH" = 'true' ]; then
		su --login --command "cd $PROJECT_PATH && cp .env.example .env" "$SERVER_USER"
	else
		su --login --command "cd $PROJECT_PATH && cp .env.example .env" "$SERVER_USER" >/dev/null 2>&1
	fi
	file="$PROJECT_PATH/.env"
	check_files "$file"
	pattern="s#^\\(WP_HOME\\)=.*#\\1=https://$PROJECT_HOSTNAME#"
	for variable in 'AUTH_KEY' 'SECURE_AUTH_KEY' 'LOGGED_IN_KEY' 'NONCE_KEY' 'AUTH_SALT' 'SECURE_AUTH_SALT' 'LOGGED_IN_SALT' 'NONCE_SALT'; do
		pattern="$pattern;#^\\($variable\\)=.*#\\1='$(password_generate 64)'"
	done
	sed --in-place "$pattern" "$file"
	sed --in-place "s/^ACF_PRO_KEY=.*/ACF_PRO_KEY=$WP_ACF_PRO_KEY/" "$file"
	sed --in-place "s/^WP_ROCKET_KEY=.*/WP_ROCKET_KEY=$WP_ROCKET_KEY/" "$file"
	sed --in-place "s/^WP_ROCKET_EMAIL=.*/WP_ROCKET_EMAIL=$WP_ROCKET_EMAIL/" "$file"
	sed --in-place "s/^WP_PLUGIN_GF_KEY=.*/WP_PLUGIN_GF_KEY=$WP_PLUGIN_GF_KEY/" "$file"

	application_file="$PROJECT_PATH/config/application.php"
	sed --in-place "s/^Config::apply();/Config::define( 'WP_ROCKET_KEY', env('WP_ROCKET_KEY'));\nConfig::define( 'WP_ROCKET_EMAIL', env('WP_ROCKET_EMAIL'));\n\nConfig::apply();/" "$application_file"

  printf 'Composer'
	if [ "$DEBUG" = 'true' ] && [ "$DEBUG_SSH" = 'true' ]; then
		su --login --command "cd $PROJECT_PATH && composer install" "$SERVER_USER"
	else
		su --login --command "cd $PROJECT_PATH && composer install" "$SERVER_USER" >/dev/null 2>&1
	fi

  printf 'Composer du thème'
	project_dir_name="${PROJECT_PATH##/*/}"
	theme_name="${project_dir_name%%.*}-theme"
	if [ "$DEBUG" = 'true' ]; then
		su --login --command "cd $PROJECT_PATH/web/app/themes/$theme_name && composer install" "$SERVER_USER"
	else
		su --login --command "cd $PROJECT_PATH/web/app/themes/$theme_name && composer install" "$SERVER_USER" >/dev/null 2>&1
	fi

	return 0
}
wordpress_initialize_project_on_server_vanilla() {
	printf 'Initialisation du projet WordPress…\n'
	if [ "$DEBUG" = 'true' ] && [ "$DEBUG_SSH" = 'true' ]; then
		su --login --command "cd $PROJECT_PATH && cp wp-config-sample.php wp-config.php" "$SERVER_USER"
	else
		su --login --command "cd $PROJECT_PATH && cp wp-config-sample.php wp-config.php" "$SERVER_USER" >/dev/null 2>&1
	fi
	file="$PROJECT_PATH/wp-config.php"
	check_files "$file"
	pattern="s#define( 'AUTH_KEY',\\s*'put your unique phrase here' );#define( 'AUTH_KEY', '$(password_generate 64)' );#"
	for key in 'AUTH_KEY' 'SECURE_AUTH_KEY' 'LOGGED_IN_KEY' 'NONCE_KEY' 'AUTH_SALT' 'SECURE_AUTH_SALT' 'LOGGED_IN_SALT' 'NONCE_SALT'; do
		pattern="$pattern;s#define( '$key',\\s*'put your unique phrase here' );#define( '$key', '$(password_generate 64)' );#"
	done
	sed --in-place "$pattern" "$file"
	return 0
}
wordpress_database_link() {
	check_variables 'PROJECT_TECH' 'SERVER_USER' 'PROJECT_NAME' 'DB_HOST' 'DB_NAME' 'DB_USER' 'DB_PASS'
	check_dependencies 'sed'
	if [ -z "$PROJECT_PATH" ]; then
		PROJECT_PATH="/home/$SERVER_USER/$PROJECT_NAME.git"
	fi
	# shellcheck disable=SC2039
	local file pattern
	case "$PROJECT_TECH" in
		('wordpress_vanilla')
			wordpress_database_link_vanilla
			;;
		(*)
			wordpress_database_link_bedrock
			;;
	esac
	if [ "$DEBUG" = 'true' ] && [ "$DEBUG_SED" = 'true' ]; then
		printf 'sed --in-place "%s" "%s"\n' "$pattern" "$file"
	fi
	sed --in-place "$pattern" "$file"
	return 0
}
wordpress_database_link_bedrock() {
	file="$PROJECT_PATH/.env"
	check_files "$file"
	pattern="s|^\\(# DB_HOST\\)=.*|DB_HOST='$DB_HOST'|"
	pattern="$pattern;s|^\\(DB_NAME\\)=.*|\\1='$DB_NAME'|"
	pattern="$pattern;s|^\\(DB_USER\\)=.*|\\1='$DB_USER'|"
	pattern="$pattern;s|^\\(DB_PASSWORD\\)=.*|\\1='$DB_PASS'|"
	return 0
}
wordpress_database_link_vanilla() {
	file="$PROJECT_PATH/wp-config.php"
	check_files "$file"
	pattern="s#define( 'DB_NAME', 'database_name_here' );#define( 'DB_NAME', '$DB_NAME' );#"
	pattern="$pattern;s#define( 'DB_USER', 'username_here' );#define( 'DB_USER', '$DB_USER' );#"
	pattern="$pattern;s#define( 'DB_PASSWORD', 'password_here' );#define( 'DB_PASSWORD', '$DB_PASS' );#"
	pattern="$pattern;s#define( 'DB_HOST', 'localhost' );#define( 'DB_HOST', '$DB_HOST' );#"
	return 0
}
wordpress_initiate_project() {
	check_variables 'LOCAL_REPO_PATH'
	case "$PROJECT_TECH" in
		('wordpress_vanilla')
			wordpress_initiate_project_vanilla
			;;
		(*)
			wordpress_initiate_project_bedrock
			;;
	esac
	printf '\033[1;32mOK\033[0m\n'
	return 0
}
wordpress_initiate_project_bedrock() {
	printf 'Démarrage dʼun projet WordPress + Bedrock, cette étape peut prendre plusieurs minutes…\n'
	check_dependencies 'composer' 'yarn'
	composer create-project --prefer-dist --no-scripts roots/bedrock "$LOCAL_REPO_PATH" >/dev/null 2>&1
	(
	# shellcheck disable=SC2164
	cd "$LOCAL_REPO_PATH"
	git init >/dev/null
	git add .
	git commit -m 'Fresh WordPress + Bedrock installation' >/dev/null
	mkdir --parents 'web/app/cache'
	cat > 'web/app/cache/.gitignore' <<- 'EOF'
		*
		!.gitignore
		EOF
		git add 'web/app/cache/.gitignore'
		git commit --message='Do not track cache files' >/dev/null
		)
		(
		# shellcheck disable=SC2164
		cd "$LOCAL_REPO_PATH"/web/app/themes
		printf 'Installation de Sage, cette étape peut prendre plusieurs minutes…\n'
		theme_name=${LOCAL_REPO_PATH##/*/}

		composer create-project --no-scripts roots/sage "$LOCAL_REPO_PATH"/web/app/themes/"${theme_name%%.*}"-theme
		cd "${theme_name%%.*}"-theme || exit
		
		# update .editoconfig
		sed --in-place 's/2/4/' '.editorconfig'
		sed --in-place 's/ space/ tab/' '.editorconfig'
		# update .stylelintrc.js
		sed --in-place "/'rules':/ a \    'number-leading-zero': null," '.stylelintrc.js'
		sed --in-place "/'rules':/ a \    'indentation': 'tab'," '.stylelintrc.js'

    echo "Composer update"
		composer update
		echo "sage meta"
		./vendor/bin/sage meta \
			--theme_name="${theme_name%%.*}" \
			--theme_uri="https://yeswedev.bzh" \
			--theme_description="Theme ${theme_name%%.*}-theme" \
			--theme_version="1.0.0" \
			--theme_author="Yes We Dev" \
			--theme_author_uri="https://yeswedev.bzh"
		# Not sure if it's ok to use the $theme_name to construct devUrl
		echo "sage config"
		./vendor/bin/sage config \
			--url="http://${theme_name%%.*}.local" \
			--path="/app/themes/${theme_name%%.*}-theme"
    echo "sage tailwind"
		./vendor/bin/sage preset --quiet --overwrite tailwind

		# delete useless style files
		cd resources/assets/styles/ || exit
		rm -rf 'components/'
		rm -rf 'layouts/'
		sed --in-place '/@import "components/d' 'main.scss'
		sed --in-place '/@import "layouts/d' 'main.scss'
		cd ../../../ || exit

		# shellcheck disable=SC2164
		echo "Yarn"
		yarn >/dev/null 2>&1
		{
			echo "ACF_PRO_KEY=xxxxx"
			echo "WP_ROCKET_KEY=xxxxx"
			echo "WP_ROCKET_EMAIL=xxxxx"
			echo "WP_PLUGIN_GF_KEY=xxxxx"
		} >> "$LOCAL_REPO_PATH"/.env.example
		file="$LOCAL_REPO_PATH/.env"
		cp "$LOCAL_REPO_PATH/.env.example" "$file"
		sed --in-place "s/^ACF_PRO_KEY=.*/ACF_PRO_KEY=$WP_ACF_PRO_KEY/" "$file"
		sed --in-place "s/^WP_ROCKET_KEY=.*/WP_ROCKET_KEY=$WP_ROCKET_KEY/" "$file"
		sed --in-place "s/^WP_ROCKET_EMAIL=.*/WP_ROCKET_EMAIL=$WP_ROCKET_EMAIL/" "$file"
		sed --in-place "s/^WP_PLUGIN_GF_KEY=.*/WP_PLUGIN_GF_KEY=$WP_PLUGIN_GF_KEY/" "$file"
		)
		(
		# shellcheck disable=SC2164
		cd "$LOCAL_REPO_PATH"
		git add .
		git commit -m 'Fresh Sage installation' >/dev/null
		# shellcheck disable=SC2039
		local controller_file theme_name placeholder_file
		theme_name="${LOCAL_REPO_PATH##/*/}"
		controller_file="web/app/themes/${theme_name%%.*}-theme/app/Controllers/App.php"
		# shellcheck disable=SC2016
		sed --in-place 's/^\(\s*\)public function siteName()/\1protected $acf = true;\n\n&/' "$controller_file"
		git add "$controller_file"
		placeholder_file="web/app/themes/${theme_name%%.*}-theme/resources/acf-json/.gitkeep"
		mkdir --parents "${placeholder_file%/*}"
		touch "$placeholder_file"
		git add "$placeholder_file"
		git commit --message='Improve Sage theme compatibility with ACF' >/dev/null
		)
		echo "Installing default plugins..."
		plugins='default-plugins.conf'
		plugins_dev='default-plugins-dev.conf'
		install_repositories='additionnaly-repositories.composer.json'
		install_extra='additionnaly-extra.composer.json'
		cp "$SCRIPT_PATH/wordpress/$plugins" "$LOCAL_REPO_PATH/$plugins"
		cp "$SCRIPT_PATH/wordpress/$plugins_dev" "$LOCAL_REPO_PATH/$plugins_dev"
		cp "$SCRIPT_PATH/wordpress/$install_repositories" "$LOCAL_REPO_PATH/$install_repositories"
		cp "$SCRIPT_PATH/wordpress/$install_extra" "$LOCAL_REPO_PATH/$install_extra"
		(
		# shellcheck disable=SC2164
		cd "$LOCAL_REPO_PATH"
		sed --in-place "/\"repositories\":/ r $install_repositories" "composer.json"
		sed --in-place "/\"extra\":/ r $install_extra" "composer.json"
		sed --in-place "/web\/app\/plugins\/\*/ a \!web/app/plugins/ywd-acf-field-group" '.gitignore'
		sed --in-place "/# Application/ a \web/app/languages/*" '.gitignore'
		while read -r line
		do
			composer require "$line"
		done < "$plugins"
		while read -r line
		do
			composer require "$line" --dev
		done < "$plugins_dev"
		rm $plugins
		rm $plugins_dev
		rm $install_repositories
		rm $install_extra
		cd "$LOCAL_REPO_PATH"/web/app/plugins || exit
		wget --no-check-certificate https://github.com/yeswedev-team/ywd-acf-field-group/archive/master.zip
		unzip master.zip
		mv ywd-acf-field-group-master/ ywd-acf-field-group
		rm master.zip
		)
		(
		# shellcheck disable=SC2164
		cd "$LOCAL_REPO_PATH"
		git add .
		git commit -m 'Plugins installed' >/dev/null
		)
		return 0
	}
	wordpress_initiate_project_vanilla() {
		printf 'Démarrage dʼun projet WordPress, cette étape peut prendre plusieurs minutes…\n'
		check_dependencies 'wget' 'dos2unix' 'find'
		# shellcheck disable=SC2039
		local download_url download_directory
		download_url='https://wordpress.org/wordpress-5.1.1.tar.gz'
		download_directory=$(mktemp --directory --tmpdir 'wordpress.XXXXX')
		wget --directory-prefix="$download_directory" "$download_url" >/dev/null 2>&1
		tar xf "$download_directory/${download_url##*/}" --directory="$download_directory"
		rm "$download_directory/${download_url##*/}"
		find "$download_directory/wordpress" -type f -name '*.php' -exec dos2unix '{}' + >/dev/null 2>&1
		mv "$download_directory"/wordpress/* "$LOCAL_REPO_PATH"
		cat > "$LOCAL_REPO_PATH/.gitignore" <<- EOF
	wp-config.php
	EOF
	(
	# shellcheck disable=SC2164
	cd "$LOCAL_REPO_PATH"
	git init >/dev/null
	git add .
	git commit -m 'Fresh WordPress 5.1.1 installation' >/dev/null
	)
	return 0
}

