check_dependencies() {
	for dependency in "$@"; do
		if ! command -v "$dependency" 1>/dev/null 2>&1; then
			echo "Erreur : la commande $dependency est introuvable."
			exit 1
		fi
	done
}
check_files() {
	for file in "$@"; do
		if [ ! -f "$file" ]; then
			echo "Erreur : le fichier $file n’existe pas."
			exit 1
		fi
	done
}
check_variables() {
	for name in "$@"; do
		# shellcheck disable=SC2086
		if [ -z "$(eval printf -- '%b' \"\$$name\")" ]; then
			echo "Erreur : la variable $name n’est pas définie."
			exit 1
		fi
	done
}
password_generate() {
	check_dependencies 'base64'
	# shellcheck disable=SC2039
	local bytes_count
	# shellcheck disable=SC2039
	local password
	bytes_count="$1"
	password="$(dd if=/dev/urandom of=/dev/stdout bs=64 count=1 2>/dev/null | base64 - | head --lines=1 | head --bytes="$bytes_count")"
	printf '%s' "$password"
	return 0
}
password_generate_simple() {
	check_dependencies 'pwgen'
	# shellcheck disable=SC2039
	local count password
	if [ "$1" ]; then
		count="$1"
	else
		count=16
	fi
	password=$(pwgen --secure $count)
	printf '%s' "$password"
	return 0
}
set_project_infos() {
	check_variables 'SERVER_BASE_URL'
	unset PROJECT_NAME
	unset PROJECT_NAME_PRETTY
	unset PROJECT_TECH
	unset PROJECT_HOSTNAME
	unset GIT_ENGINE
	unset REAL_TIME_CHAT_SERVICE
	unset WIKI_ENGINE
	while [ -z "$PROJECT_TECH" ]; do
		printf '%s\n' 'Technologie du projet :'
		printf '1 — Laravel\n'
		printf '2 — WordPress + Bedrock\n'
		printf '3 — WordPress vanilla\n'
		printf '4 — PrestaShop\n'
		printf '5 — PrestaShop 1.6\n'
		printf '6 — Drupal\n'
		printf '7 — %s\n' 'custom'
		printf '%s\n' 'Entrez le numéro correspondant.'
		read -r PROJECT_TECH
		case "$PROJECT_TECH" in
			('1')
				PROJECT_TECH='laravel'
			;;
			('2')
				PROJECT_TECH='wordpress'
				check_dependencies 'yarn'
			;;
			('3')
				PROJECT_TECH='wordpress_vanilla'
				check_dependencies 'dos2unix'
			;;
			('4')
				PROJECT_TECH='prestashop'
			;;
			('5')
				PROJECT_TECH='prestashop1.6'
			;;
			('6')
				PROJECT_TECH='drupal'
			;;
			('7')
				PROJECT_TECH='custom'
			;;
			(*)
				# shellcheck disable=SC1112
				printf '"%s" %s\n' "$PROJECT_TECH" 'n’est pas une valeur acceptée.'
				unset PROJECT_TECH
			;;
		esac
	done
	while [ -z "$PROJECT_NAME_PRETTY" ]; do
		printf '%s\n' 'Entrez le nom du projet :'
		read -r PROJECT_NAME_PRETTY
		if [ -n "$PROJECT_NAME_PRETTY" ]; then
			PROJECT_NAME="$(printf '%s' "$PROJECT_NAME_PRETTY" | tr '[:upper:]' '[:lower:]' | sed 's/[ _]/-/g;s/[áàäâ]/a/g;s/[éèëê]/e/g;s/[íìïî]/i/g;s/[óòöô]/o/g;s/[úùüû]/u/g;s/[ýỳÿŷ]/y/g')"
		fi
	done

	GIT_ENGINE='gitlab'
  check_dependencies 'jq'
  printf '"%s" %s\n' "$GIT_ENGINE" 'utilisé.'

	while [ -z "$REAL_TIME_CHAT_SERVICE" ]; do
		printf '%s\n' 'Service de chat en temps réel :'
		printf '1 — Rocket.Chat+\n'
		printf '2 — aucun\n'
		printf '%s\n' 'Entrez le numéro correspondant.'
		read -r REAL_TIME_CHAT_SERVICE
		case "$REAL_TIME_CHAT_SERVICE" in
			('1')
				REAL_TIME_CHAT_SERVICE='rocketchat'
			;;
			('2')
				REAL_TIME_CHAT_SERVICE='none'
			;;
			(*)
				# shellcheck disable=SC1112
				printf '"%s" %s\n' "$REAL_TIME_CHAT_SERVICE" 'n’est pas une valeur acceptée.'
				unset REAL_TIME_CHAT_SERVICE
			;;
		esac
	done
	while [ -z "$WIKI_ENGINE" ]; do
		printf '%s\n' 'Moteur de wiki :'
		printf '1 — DokuWiki\n'
		printf '2 — aucun\n'
		printf '%s\n' 'Entrez le numéro correspondant.'
		read -r WIKI_ENGINE
		case "$WIKI_ENGINE" in
			('1')
				WIKI_ENGINE='dokuwiki'
			;;
			('2')
				WIKI_ENGINE='none'
			;;
			(*)
				# shellcheck disable=SC1112
				printf '"%s" %s\n' "$WIKI_ENGINE" 'n’est pas une valeur acceptée.'
				unset WIKI_ENGINE
			;;
		esac
	done
	PROJECT_HOSTNAME="$PROJECT_NAME.$SERVER_BASE_URL"
	export PROJECT_NAME
	export PROJECT_NAME_PRETTY
	export PROJECT_TECH
	export PROJECT_HOSTNAME
	export GIT_ENGINE
	export REAL_TIME_CHAT_SERVICE
	export WIKI_ENGINE
	cat > "$SCRIPT_PATH/config/project.info" <<- EOF
	PROJECT_NAME='$PROJECT_NAME'
	PROJECT_NAME_PRETTY='$PROJECT_NAME_PRETTY'
	PROJECT_TECH='$PROJECT_TECH'
	PROJECT_HOSTNAME='$PROJECT_HOSTNAME'
	GIT_ENGINE='$GIT_ENGINE'
	WIKI_ENGINE='$WIKI_ENGINE'
	EOF
}
parse_configuration() {
	if [ -e "$SCRIPT_PATH/config/project.info" ]; then
		# shellcheck disable=SC1090
		. "$SCRIPT_PATH/config/project.info"
		export PROJECT_NAME PROJECT_NAME_PRETTY PROJECT_TECH PROJECT_HOSTNAME GIT_ENGINE WIKI_ENGINE HTTP_USER HTTP_PASS DB_HOST DB_NAME DB_USER DB_PASS
		if [ "$DEBUG" = 'true' ] && [ "$DEBUG_VARIABLES" = 'true' ]; then
			printf 'Utilisation de %s\n' "$SCRIPT_PATH/config/project.info"
			printf 'PROJECT_NAME="%s"\n' "$PROJECT_NAME"
			printf 'PROJECT_NAME_PRETTY="%s"\n' "$PROJECT_NAME_PRETTY"
			printf 'PROJECT_TECH="%s"\n' "$PROJECT_TECH"
			printf 'PROJECT_HOSTNAME="%s"\n' "$PROJECT_HOSTNAME"
			printf 'GIT_ENGINE="%s"\n' "$GIT_ENGINE"
			printf 'WIKI_ENGINE="%s"\n' "$WIKI_ENGINE"
			printf 'HTTP_USER="%s"\n' "$HTTP_USER"
			printf 'HTTP_PASS="%s"\n' "$HTTP_PASS"
			printf 'DB_HOST="%s"\n' "$DB_HOST"
			printf 'DB_NAME="%s"\n' "$DB_NAME"
			printf 'DB_USER="%s"\n' "$DB_USER"
			printf 'DB_PASS="%s"\n' "$DB_PASS"
		fi
	fi

	if [ -e "$SCRIPT_PATH/config/debug.conf" ]; then
		# shellcheck source=config/debug.conf.example
		. "$SCRIPT_PATH/config/debug.conf"
		export DEBUG DEBUG_GENERIC DEBUG_CURL DEBUG_GIT DEBUG_SED DEBUG_SSH DEBUG_VARIABLES
		if [ "$DEBUG" = 'true' ] && [ "$DEBUG_VARIABLES" = 'true' ]; then
			printf 'Utilisation de %s\n' "$SCRIPT_PATH/config/debug.conf"
			printf 'DEBUG="%s"\n' "$DEBUG"
			printf 'DEBUG_GENERIC="%s"\n' "$DEBUG_GENERIC"
			printf 'DEBUG_CURL="%s"\n' "$DEBUG_CURL"
			printf 'DEBUG_GIT="%s"\n' "$DEBUG_GIT"
			printf 'DEBUG_SED="%s"\n' "$DEBUG_SED"
			printf 'DEBUG_SSH="%s"\n' "$DEBUG_SSH"
			printf 'DEBUG_VARIABLES="%s"\n' "$DEBUG_VARIABLES"
		fi
	else
		DEBUG='false'
		export DEBUG
	fi

	check_files "$SCRIPT_PATH/config/server.conf"
	# shellcheck source=config/server.conf.example
	. "$SCRIPT_PATH/config/server.conf"
	export SERVER_BASE_URL SERVER_HOSTNAME SERVER_USER SERVER_SSH_PORT
	if [ "$DEBUG" = 'true' ] && [ "$DEBUG_VARIABLES" = 'true' ]; then
		printf 'Utilisation de %s\n' "$SCRIPT_PATH/config/server.conf"
		printf 'SERVER_BASE_URL="%s"\n' "$SERVER_BASE_URL"
		printf 'SERVER_HOSTNAME="%s"\n' "$SERVER_HOSTNAME"
		printf 'SERVER_USER="%s"\n' "$SERVER_USER"
		printf 'SERVER_SSH_PORT="%s"\n' "$SERVER_SSH_PORT"
	fi

	if [ -n "$REAL_TIME_CHAT_SERVICE" ] && [ "$REAL_TIME_CHAT_SERVICE" != 'none' ]; then
		check_files "$SCRIPT_PATH/config/realtimechat.conf"
		# shellcheck source=config/realtimechat.conf.example
		. "$SCRIPT_PATH/config/realtimechat.conf"
		export ROCKETCHAT_API_SERVER ROCKETCHAT_API_USERID ROCKETCHAT_API_TOKEN
		if [ "$DEBUG" = 'true' ] && [ "$DEBUG_VARIABLES" = 'true' ]; then
			printf 'Utilisation de %s\n' "$SCRIPT_PATH/config/realtimechat.conf"
			printf 'ROCKETCHAT_API_SERVER="%s"\n' "$ROCKETCHAT_API_SERVER"
			printf 'ROCKETCHAT_API_USERID="%s"\n' "$ROCKETCHAT_API_USERID"
			printf 'ROCKETCHAT_API_TOKEN="%s"\n' "$ROCKETCHAT_API_TOKEN"
		fi
	fi

	if [ -n "$WIKI_ENGINE" ] && [ "$WIKI_ENGINE" != 'none' ]; then
		check_files "$SCRIPT_PATH/config/wiki.conf"
		# shellcheck source=config/wiki.conf.example
		. "$SCRIPT_PATH/config/wiki.conf"
		export DOKUWIKI_SERVER_HOST DOKUWIKI_SERVER_USER DOKUWIKI_SERVER_PORT DOKUWIKI_PAGES_PATH DOKUWIKI_INDEX_URL
		if [ "$DEBUG" = 'true' ] && [ "$DEBUG_VARIABLES" = 'true' ]; then
			printf 'Utilisation de %s\n' "$SCRIPT_PATH/config/wiki.conf"
			printf 'DOKUWIKI_SERVER_HOST="%s"\n' "$DOKUWIKI_SERVER_HOST"
			printf 'DOKUWIKI_SERVER_USER="%s"\n' "$DOKUWIKI_SERVER_USER"
			printf 'DOKUWIKI_SERVER_PORT="%s"\n' "$DOKUWIKI_SERVER_PORT"
			printf 'DOKUWIKI_PAGES_PATH="%s"\n' "$DOKUWIKI_PAGES_PATH"
			printf 'DOKUWIKI_INDEX_URL="%s"\n' "$DOKUWIKI_INDEX_URL"
		fi
	fi

	case "$GIT_ENGINE" in
		('gitlab')
			check_files "$SCRIPT_PATH/config/gitlab.conf"
			# shellcheck source=config/gitlab.conf.example
			. "$SCRIPT_PATH/config/gitlab.conf"
			export GITLAB_BASE_URL GITLAB_ACCESS_TOKEN GITLAB_NAMESPACE_ID GITLAB_SSH_KEY_ID
			if [ "$DEBUG" = 'true' ] && [ "$DEBUG_VARIABLES" = 'true' ]; then
				printf 'Utilisation de %s\n' "$SCRIPT_PATH/config/gitlab.conf"
				printf 'GITLAB_BASE_URL="%s"\n' "$GITLAB_BASE_URL"
				printf 'GITLAB_ACCESS_TOKEN="%s"\n' "$GITLAB_ACCESS_TOKEN"
				printf 'GITLAB_NAMESPACE_ID="%s"\n' "$GITLAB_NAMESPACE_ID"
				printf 'GITLAB_SSH_KEY_ID="%s"\n' "$GITLAB_SSH_KEY_ID"
			fi
		;;
	esac

	case "$PROJECT_TECH" in
		('prestashop'*)
			check_files "$SCRIPT_PATH/config/database.conf"
			# shellcheck source=config/database.conf.example
			. "$SCRIPT_PATH/config/database.conf"
			export LOCAL_DB_HOST LOCAL_DB_NAME LOCAL_DB_USER LOCAL_DB_PASS
			if [ "$DEBUG" = 'true' ] && [ "$DEBUG_VARIABLES" = 'true' ]; then
				printf 'Utilisation de %s\n' "$SCRIPT_PATH/config/database.conf"
				printf 'LOCAL_DB_HOST="%s"\n' "$LOCAL_DB_HOST"
				printf 'LOCAL_DB_NAME="%s"\n' "$LOCAL_DB_NAME"
				printf 'LOCAL_DB_USER="%s"\n' "$LOCAL_DB_USER"
				printf 'LOCAL_DB_PASS="%s"\n' "$LOCAL_DB_PASS"
			fi
		;;
		('wordpress')
			check_files "$SCRIPT_PATH/config/wordpress.conf"
			# shellcheck source=config/wordpress.conf.example
			. "$SCRIPT_PATH/config/wordpress.conf"
			export WP_ACF_PRO_KEY
			export WP_ROCKET_KEY
			export WP_ROCKET_EMAIL
			export WP_PLUGIN_GF_KEY
			if [ "$DEBUG" = 'true' ] && [ "$DEBUG_VARIABLES" = 'true' ]; then
				printf 'Utilisation de %s\n' "$SCRIPT_PATH/config/wordpress.conf"
				printf 'WP_ACF_PRO_KEY="%s"\n' "$WP_ACF_PRO_KEY"
				printf 'WP_ROCKET_KEY="%s"\n' "$WP_ROCKET_KEY"
				printf 'WP_ROCKET_EMAIL="%s"\n' "$WP_ROCKET_EMAIL"
				printf 'WP_PLUGIN_GF_KEY="%s"\n' "$WP_PLUGIN_GF_KEY"
			fi

		;;
	esac
}
send_api_call() {
	curl_options="$1"
	curl_url="$2"
	check_variables 'curl_options' 'curl_url'
	check_dependencies 'curl'
	if [ "$DEBUG" = 'true' ] && [ "$DEBUG_CURL" = 'true' ]; then
		echo curl "$curl_options" \""$curl_url"\"
	fi
	# shellcheck disable=SC2046,SC2086,SC2116
	API_FEEDBACK="$(eval $(echo curl $curl_options "$curl_url") 2>/dev/null)"
	export API_FEEDBACK
	if [ "$DEBUG" = 'true' ] && [ "$DEBUG_CURL" = 'true' ]; then
		echo "$API_FEEDBACK"
	fi
	return 0
}
ssh_get_keys() {
	check_variables 'SSH_PUBLIC_KEY' 'SERVER_USER' 'SERVER_HOSTNAME'
	check_dependencies 'rsync'
	mkdir --parents "$(dirname "$SSH_PUBLIC_KEY")"
	rsync $SERVER_USER@$SERVER_HOSTNAME:.ssh/id_rsa.pub "$SSH_PUBLIC_KEY"
	rsync $SERVER_USER@$SERVER_HOSTNAME:.ssh/id_rsa "${SSH_PUBLIC_KEY%.pub}"
	check_files "$SSH_PUBLIC_KEY" "${SSH_PUBLIC_KEY%.pub}"
	return 0
}
