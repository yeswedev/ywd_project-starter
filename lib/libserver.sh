server_database_create() {
	check_variables 'PROJECT_NAME'
	check_dependencies 'mysql'
	DB_HOST='localhost'
	DB_NAME="$(printf '%s' "$PROJECT_NAME" | sed 's/[-.]/_/g')"
	DB_USER="$(printf '%s' "$PROJECT_NAME" | sed 's/[-.]/_/g')"
	DB_PASS="$(password_generate 32)"
	mysql --user='root' <<- EOF
	create database $DB_NAME;
	grant all privileges on $DB_NAME.* to '$DB_USER'@'$DB_HOST' identified by '$DB_PASS';
	flush privileges;
	EOF
	export DB_HOST DB_NAME DB_USER DB_PASS
	if [ "$WIKI_ENGINE" = 'none' ]; then
		printf '#####\n'
		printf '# Accès à la base de données\n'
		printf '# Hôte : "%s"\n' "$DB_HOST"
		printf '# Base : "%s"\n' "$DB_NAME"
		printf '# Utilisateur : "%s"\n' "$DB_USER"
		printf '# Mot de passe : "%s"\n' "$DB_PASS"
		printf '#####\n'
	fi
	cat >> "$SCRIPT_PATH/config/project.info" <<- EOF
	DB_HOST='$DB_HOST'
	DB_NAME='$DB_NAME'
	DB_USER='$DB_USER'
	DB_PASS='$DB_PASS'
	EOF
	if [ "$DEBUG"  = 'true' ]; then
		echo "DB_HOST='$DB_HOST'"
		echo "DB_NAME='$DB_NAME'"
		echo "DB_USER='$DB_USER'"
		echo "DB_PASS='$DB_PASS'"
	fi
	return 0
}
server_set_up_toolbox() {
	printf 'Mise en place de la boîte à outils Yes We Dev sur le serveur…\n'
	check_variables 'SERVER_USER' 'PROJECT_NAME' 'PROJECT_TECH' 'PROJECT_HOSTNAME'
	check_dependencies 'git' 'sed'
	# shellcheck disable=SC2039
	local pattern
	pattern="s#^\\(PROJECT_NAME\\)=.*#\\1='$PROJECT_NAME'#"
	pattern="$pattern;s#^\\(PROJECT_TECH\\)=.*#\\1='$PROJECT_TECH'#"
	case "$PROJECT_TECH" in
		('laravel')
			pattern="$pattern;s#^\\(ARTISAN_MIGRATE\\)=.*#\\1='false'#"
			pattern="$pattern;s#^\\(ARTISAN_REFRESH\\)=.*#\\1='true'#"
			pattern="$pattern;s#^\\(ARTISAN_SEED\\)=.*#\\1='true'#"
		;;
		('drupal')
			# Should no longer be disabled once link with database is automated
			pattern="$pattern;s#^\\(DB_BACKUP\\)=.*#\\1='false'#"
		;;
		('custom')
			pattern="$pattern;s#^\\(DB_BACKUP\\)=.*#\\1='false'#"
		;;
	esac
	# shellcheck disable=SC2039
	local project_path
	project_path="/home/$SERVER_USER/$PROJECT_NAME.git"
	su --login --command "cd \"$project_path\" && \
		git submodule update --recursive --init ywd_deploy && \
		cp ywd_deploy/toolbox.conf.example ywd_deploy/toolbox.conf && \
		sed --in-place \"$pattern\" ywd_deploy/toolbox.conf" "$SERVER_USER" >/dev/null 2>&1
	# shellcheck disable=SC2039
	local deploy_script
	deploy_script="/usr/local/bin/deploy_$PROJECT_NAME"
	mkdir --parents "$(dirname "$deploy_script")"
	cat > "$deploy_script" <<- EOF
	#!/bin/sh
	set -o errexit

	PROJECT_PATH='$project_path'

	if [ "\$(id --user)" = 0 ]; then
	    su --command "\$PROJECT_PATH/ywd_deploy/deploy.sh" '$SERVER_USER'
	else
	    "\$PROJECT_PATH/ywd_deploy/deploy.sh"
	fi

	exit 0
	EOF
	chmod 755 "$deploy_script"
	printf '\033[1;32mOK\033[0m\n'
	if [ "$WIKI_ENGINE" = 'none' ]; then
		printf '#####\n'
		printf '# Déploiement\n'
		# shellcheck disable=SC2016
		printf '# `ssh %s@%s %s`\n' "$SERVER_USER" "$PROJECT_HOSTNAME" "$(basename "$deploy_script")"
		printf '#####\n'
	fi
}

