real_time_chat_create_channel() {
	check_variables 'REAL_TIME_CHAT_SERVICE'
	case "$REAL_TIME_CHAT_SERVICE" in
		('rocketchat')
			rocketchat_set_service
			rocketchat_create_channel
		;;
		('none')
			true
		;;
		(*)
			real_time_chat_unknown_service
		;;
	esac
}
real_time_chat_unknown_service() {
	# shellcheck disable=SC1112
	printf 'Erreur : le service "%s" n’est pas géré.\n' "$REAL_TIME_CHAT_SERVICE"
	return 1
}

