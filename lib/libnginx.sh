nginx_create_website_configuration() {
	printf 'Mise en place de la configuration Nginx…\n'
	check_variables 'PROJECT_HOSTNAME' 'PROJECT_TECH' 'PROJECT_NAME' 'SERVER_USER'
	case "$PROJECT_TECH" in
		('prestashop'*)
			check_variables 'PRESTASHOP_ADMIN_PATH'
		;;
	esac
	check_dependencies 'service' 'htpasswd'
	# shellcheck disable=SC2039
	local nginx_conf_file
	case "$PROJECT_TECH" in
		('laravel')
			HTTP_ROOT="/srv/http/$PROJECT_HOSTNAME/public"
		;;
		('wordpress'|'drupal')
			HTTP_ROOT="/srv/http/$PROJECT_HOSTNAME/web"
		;;
		(*)
			HTTP_ROOT="/srv/http/$PROJECT_HOSTNAME"
		;;
	esac
	# shellcheck disable=SC2039
	local php_version
	case "$PROJECT_TECH" in
		('pretashop1.6')
			php_version='7.1'
		;;
		(*)
			php_version='7.3'
		;;
	esac
	printf 'Authentification HTTP\n'
	HTTP_AUTH_FILE="/srv/http/htpasswd_$PROJECT_NAME"
	HTTP_USER="$SERVER_USER"
	HTTP_PASS="$(password_generate_simple 8)"
	htpasswd -i -c "$HTTP_AUTH_FILE" "$HTTP_USER" >/dev/null 2>&1 <<- EOF
	$HTTP_PASS
	EOF
	printf 'Site Web temporaire\n'
	mkdir --parents "$HTTP_ROOT"
	cat > "$HTTP_ROOT/index.html" <<- EOF
	<h1>It works!</h1>
	<p>Project: $PROJECT_NAME</p>
	EOF
	printf 'Répertoire de fichiers journaux\n'
	HTTP_LOGS=/var/log/nginx/$PROJECT_HOSTNAME
	mkdir --parents "$HTTP_LOGS"
	chown www-data.adm "$HTTP_LOGS"
	chmod ug+rwxs "$HTTP_LOGS"
	printf 'Configuration du site Web\n'
	nginx_conf_file="/etc/nginx/sites-available/$PROJECT_HOSTNAME"
	cat > "$nginx_conf_file" <<- EOF
	server {
	    server_name $PROJECT_HOSTNAME;

	    listen 80;
	    listen [::]:80;

	#	return 301 https://\$host\$request_uri;
	#}

	#server {
	#	server_name $PROJECT_HOSTNAME;

	#	listen 443 ssl http2;
	#	listen [::]:443 ssl http2;

	#	ssl_certificate     /etc/letsencrypt/live/$PROJECT_HOSTNAME/fullchain.pem;
	#	ssl_certificate_key /etc/letsencrypt/live/$PROJECT_HOSTNAME/privkey.pem;

	    root $HTTP_ROOT;

	    access_log $HTTP_LOGS/access.log;
	    error_log  $HTTP_LOGS/error.log;

	    auth_basic "Zone restreinte";
	    auth_basic_user_file $HTTP_AUTH_FILE;

	    index index.php index.html index.htm;

	    gzip on;
	    gzip_vary on;
	    gzip_proxied any;
	    gzip_comp_level 6;
	    gzip_buffers 16 8k;
	    gzip_http_version 1.1;
	    gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript;

	EOF
	case "$PROJECT_TECH" in
		('prestashop'*)
		cat >> "$nginx_conf_file" <<- EOF
		    location /$PRESTASHOP_ADMIN_PATH/ {
		        if (!-e \$request_filename) {
		            rewrite ^/.*$ /$PRESTASHOP_ADMIN_PATH/index.php last;
		        }
		    }

		    location /api/ {
			rewrite ^/api/?(.*)$ /webservice/dispatcher.php?url=\$1 last;
		    }

		EOF
		;;
	esac
	cat >> "$nginx_conf_file" <<- EOF
	    location / {
	        try_files \$uri \$uri/ /index.php?\$query_string;
	    }

	    error_page 404 /index.php;

	    location = /favicon.ico {
	        access_log off;
	        log_not_found off;
	    }
	    location = /robots.txt {
	        access_log off;
	        log_not_found off;
	    }

	    location ~ \\.php$ {
	        include snippets/fastcgi-php.conf;
	        fastcgi_pass unix:/run/php/php${php_version}-fpm.sock;
	    }

	    location ~ /\\.(?!well-known).* {
	        deny all;
	    }
	}
	EOF
	sed --in-place 's/    /\t/g' "$nginx_conf_file"
	ln --symbolic "../sites-available/$PROJECT_HOSTNAME" /etc/nginx/sites-enabled
	service nginx reload
	# shellcheck disable=SC1112
	printf 'Script de gestion de l’authentification HTTP\n'
	http_authentication_script="/usr/local/bin/http-authentication_$PROJECT_NAME"
	cat > "$http_authentication_script" <<- EOF
	#!/bin/sh

	print_usage() {
	    printf 'USAGE: %s enable|disable\\n' "\$(basename "\$0")"
	}

	is_auth_enabled() {
	    grep --regex='^	auth_basic' "\$1" >/dev/null
	}

	enable_auth() {
	    if is_auth_enabled "\$1"; then
	        return 0;
	    else
	        sed --in-place 's/^#\\(	auth_basic\\)/\\1/' "\$1"
	    fi
	}

	disable_auth() {
	    if ! is_auth_enabled "\$1"; then
	        return 0;
	    else
	        sed --in-place 's/^\\(	auth_basic\\)/#\\1/' "\$1"
	    fi
	}

	nginx_reload() {
	    if nginx -t >/dev/null 2>&1; then
	        service nginx reload
	    fi
	}

	if [ "\$#" != 1 ]; then
	    print_usage
	    exit 1
	fi

	nginx_conf_file="$nginx_conf_file"

	case "\$1" in
	    ('enable')
	        enable_auth "\$nginx_conf_file"
	        nginx_reload
	    ;;
	    ('disable')
	        disable_auth "\$nginx_conf_file"
	        nginx_reload
	    ;;
	    (*)
	        print_usage
	        exit 1
	    ;;
	esac

	exit 0
	EOF
	sed --in-place 's/    /\t/g' "$http_authentication_script"
	chmod +x "$http_authentication_script"
	printf '\033[1;32mOK\033[0m\n'
	if [ "$WIKI_ENGINE" = 'none' ]; then
		printf '#####\n'
		printf '# Accès au site\n'
		printf '# URL : %s\n' "https://$PROJECT_HOSTNAME/"
		printf '#####\n'
		printf '# Authentification HTTP\n'
		printf '# Utilisateur : "%s"\n' "$HTTP_USER"
		printf '# Mot de passe : "%s"\n' "$HTTP_PASS"
		# shellcheck disable=SC2016
		printf '# Désactivation : `ssh root@%s %s disable`\n' "$PROJECT_HOSTNAME" "$(basename "$http_authentication_script")"
		# shellcheck disable=SC2016
		printf '# Activation : `ssh root@%s %s enable`\n' "$PROJECT_HOSTNAME" "$(basename "$http_authentication_script")"
		printf '#####\n'
	fi
	cat >> "$SCRIPT_PATH/config/project.info" <<- EOF
	HTTP_USER='$HTTP_USER'
	HTTP_PASS='$HTTP_PASS'
	EOF
}
nginx_enable_https_with_certbot() {
	printf 'Mise en place du certificat TLS avec certbot…\n'
	check_variables 'PROJECT_HOSTNAME'
	check_dependencies 'certbot'
	if [ "$DEBUG" = 'true' ] && [ "$DEBUG_GENERIC" = 'true' ]; then
		printf 'certbot certonly --nginx --agree-tos --keep-until-expiring -d "%s"\n' "$PROJECT_HOSTNAME"
		certbot certonly --nginx --agree-tos --keep-until-expiring -d "$PROJECT_HOSTNAME"
	else
		certbot certonly --nginx --agree-tos --keep-until-expiring -d "$PROJECT_HOSTNAME" >/dev/null 2>&1
	fi
	case "$PROJECT_TECH" in
		('prestashop'*)
			# Do not enable HTTP → HTTPS redirection for PrestaShop projects
			# shellcheck disable=SC2039
			local pattern
			pattern='s/^#\(\tlisten\)/\1/'
			pattern="$pattern;"'s/^#\(\tssl_certificate\)/\1/'
			sed --in-place "$pattern" "/etc/nginx/sites-available/$PROJECT_HOSTNAME"
		;;
		(*)
			sed --in-place 's/^#//' "/etc/nginx/sites-available/$PROJECT_HOSTNAME"
		;;
	esac
	service nginx reload
	printf '\033[1;32mOK\033[0m\n'
}

