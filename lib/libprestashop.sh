prestashop_initiate_project() {
	check_variables 'PROJECT_TECH'
	# shellcheck disable=SC1112
	printf 'Démarrage d’un projet PrestaShop, cette étape peut prendre plusieurs minutes…\n'
	check_dependencies 'wget' 'unzip'
	# shellcheck disable=SC2039
	local prestashop_download_directory
	# shellcheck disable=SC2039
	local prestashop_download_url
	case "$PROJECT_TECH" in
		('prestashop1.6')
			prestashop_download_url='https://download.prestashop.com/download/releases/prestashop_1.6.1.23.zip'
		;;
		(*)
			prestashop_download_url='https://download.prestashop.com/download/releases/prestashop_1.7.5.1.zip'
		;;
	esac
	prestashop_download_directory="$(mktemp --directory --tmpdir="${TMPDIR:-/tmp}" "prestashop.XXXXX")"
	wget --directory-prefix="$prestashop_download_directory" "$prestashop_download_url" >/dev/null 2>&1
	case "$PROJECT_TECH" in
		('prestashop1.6')
			unzip "$prestashop_download_directory/$(basename "$prestashop_download_url")"  'prestashop/*' -d "$prestashop_download_directory" >/dev/null
			rm "$prestashop_download_directory/$(basename "$prestashop_download_url")"
			mv "$prestashop_download_directory/prestashop"/* "$LOCAL_REPO_PATH"
		;;
		(*)
			unzip "$prestashop_download_directory/$(basename "$prestashop_download_url")" 'prestashop.zip' -d "$prestashop_download_directory" >/dev/null
			rm "$prestashop_download_directory/$(basename "$prestashop_download_url")"
			unzip "$prestashop_download_directory/prestashop.zip" -d "$LOCAL_REPO_PATH" >/dev/null
		;;
	esac
	rm --force --recursive "$prestashop_download_directory"
	(
		# shellcheck disable=SC2164
		cd "$LOCAL_REPO_PATH"
		for directory in 'var/cache/prod' 'var/logs'; do
			mkdir --parents "$directory"
			cat >> "$directory/.gitignore" <<- EOF
			*
			!/.gitignore
			EOF
		done
		cat > 'app/config/.gitignore' <<- EOF
		/parameters.php
		/parameters.yml
		EOF
		cat > '.gitignore' <<- EOF
		/.htaccess
		EOF
		mkdir --parents 'config/themes/classic'
		cat > 'config/themes/classic/.gitignore' <<- EOF
		/shop1.json
		EOF
		git init >/dev/null
		git add .
		# shellcheck disable=SC2039
		local commit_message
		case "$PROJECT_TECH" in
			('prestashop1.6')
				commit_message='Fresh PrestaShop 1.6 installation'
			;;
			(*)
				commit_message='Fresh PrestaShop installation'
			;;
		esac
		if [ "$DEBUG" = 'true' ] && [ "$DEBUG_GIT" = 'true' ] ; then
			git commit --message="$commit_message"
		else
			git commit --message="$commit_message" >/dev/null 2>&1
		fi
		# shellcheck disable=SC2030
		PROJECT_PATH="$LOCAL_REPO_PATH"
		# shellcheck disable=SC2030
		SERVER_USER=$(id --user --name)
		# shellcheck disable=SC2030
		DB_HOST="$LOCAL_DB_HOST"
		# shellcheck disable=SC2030
		DB_USER="$LOCAL_DB_USER"
		# shellcheck disable=SC2030
		DB_PASS="$LOCAL_DB_PASS"
		# shellcheck disable=SC2030
		DB_NAME="$LOCAL_DB_NAME"
		prestashop_initial_installation
		git checkout -- 'var/cache/prod/.gitignore'
		git add .
		if [ "$DEBUG" = 'true' ] && [ "$DEBUG_GIT" = 'true' ] ; then
			git commit --message='Initial PrestaShop configuration'
		else
			git commit --message='Initial PrestaShop configuration' >/dev/null 2>&1
		fi
	)
	PRESTASHOP_ADMIN_PATH="$(mktemp --dry-run adminXXXXX | tr '[:upper:]' '[:lower:]')"
	export PRESTASHOP_ADMIN_PATH
	cat >> "$SCRIPT_PATH/config/project.info" <<- EOF
	PRESTASHOP_ADMIN_PATH='$PRESTASHOP_ADMIN_PATH'
	EOF
	printf '\033[1;32mOK\033[0m\n'
}
prestashop_initialize_project_on_server() {
	printf 'Initialisation du projet PrestaShop…\n'
	check_dependencies 'composer'
	check_variables 'PROJECT_NAME' 'PROJECT_TECH'
	# shellcheck disable=SC2031
	if [ -z "$PROJECT_PATH" ]; then
		PROJECT_PATH="/home/$SERVER_USER/$PROJECT_NAME.git"
	fi
	for directory in 'config' 'download' 'img' 'mails' 'modules' 'themes' 'translations' 'upload' 'var'; do
		chgrp --recursive 'www-data' "$PROJECT_PATH/$directory"
		chmod --recursive ug+rwX "$PROJECT_PATH/$directory"
	done
	case "$PROJECT_TECH" in
		('prestashop1.6')
			for directory in 'cache' 'log'; do
				chgrp --recursive 'www-data' "$PROJECT_PATH/$directory"
				chmod --recursive ug+rwX "$PROJECT_PATH/$directory"
			done
		;;
		(*)
			for directory in 'app/config' 'app/Resources/translations'; do
				chgrp 'www-data' "$PROJECT_PATH/$directory"
				chmod ug+rwX "$PROJECT_PATH/$directory"
			done
		;;
	esac
	printf '\033[1;32mOK\033[0m\n'
}
prestashop_database_link() {
	check_variables 'SERVER_USER' 'PROJECT_NAME'
	# shellcheck disable=SC2039
	local PROJECT_PATH
	# shellcheck disable=SC2031
	PROJECT_PATH="/home/$SERVER_USER/$PROJECT_NAME.git"
	printf 'Configuration initiale de PrestaShop sur le serveur…\n'
	prestashop_initial_installation
	# shellcheck disable=SC2031
	# shellcheck disable=SC2086
	chown --recursive $SERVER_USER.www-data "$PROJECT_PATH"
	# shellcheck disable=SC2031
	su --login --command "cd $PROJECT_PATH && git checkout -- ." "$SERVER_USER" >/dev/null 2>&1
	printf '\033[1;32mOK\033[0m\n'
	check_variables 'PRESTASHOP_EMAIL' 'PRESTASHOP_PASSWORD'
	cat >> "$SCRIPT_PATH/config/project.info" <<- EOF
	PRESTASHOP_EMAIL='$PRESTASHOP_EMAIL'
	PRESTASHOP_PASSWORD='$PRESTASHOP_PASSWORD'
	EOF
	if [ "$WIKI_ENGINE" = 'none' ]; then
		printf '#####\n'
		printf '# Accès au back-office PrestaShop\n'
		printf '# Utilisateur : "%s"\n' "$PRESTASHOP_EMAIL"
		printf '# Mot de passe : "%s"\n' "$PRESTASHOP_PASSWORD"
		printf '#####\n'
	fi
	return 0
}
prestashop_initial_installation() {
	check_variables 'PROJECT_TECH' 'SERVER_USER' 'PROJECT_NAME' 'PROJECT_HOSTNAME' 'DB_HOST' 'DB_USER' 'DB_PASS' 'DB_NAME' 'PROJECT_NAME_PRETTY' 'PROJECT_PATH'
	check_dependencies
	# shellcheck disable=SC2039
	local php_cli
	case "$PROJECT_TECH" in
		('prestashop1.6')
			php_cli='php7.1'
		;;
		(*)
			php_cli='php7.2'
		;;
	esac
	check_dependencies "$php_cli"
	PRESTASHOP_PASSWORD="$(password_generate 16)"
	# shellcheck disable=SC2031
	PRESTASHOP_EMAIL="$SERVER_USER@$PROJECT_HOSTNAME"
	# shellcheck disable=SC2039
	local install_options
	install_options='--language=fr'
	install_options="$install_options --timezone=Europe/Paris"
	install_options="$install_options --domain=$PROJECT_HOSTNAME"
	# shellcheck disable=SC2031
	install_options="$install_options --db_server=$DB_HOST"
	# shellcheck disable=SC2031
	install_options="$install_options --db_user=$DB_USER"
	# shellcheck disable=SC2031
	install_options="$install_options --db_password=$DB_PASS"
	# shellcheck disable=SC2031
	install_options="$install_options --db_name=$DB_NAME"
	install_options="$install_options --country=fr"
	install_options="$install_options --password=$PRESTASHOP_PASSWORD"
	install_options="$install_options --email=$PRESTASHOP_EMAIL"
	install_options="$install_options --newsletter=0"
	case "$PROJECT_TECH" in
		('prestashop')
			install_options="$install_options --ssl=1"
		;;
	esac
	# shellcheck disable=SC2086
	if ! $php_cli "$PROJECT_PATH/install/index_cli.php" --name="$PROJECT_NAME_PRETTY" $install_options >/dev/null; then
		printf 'Erreur lors de lʼinstallation initiale de PrestaShop.\n'
		printf 'Vérifiez que les identifiants utilisés pour accéder à la base de données sont corrects :\n'
		# shellcheck disable=SC2031
		printf 'db_server="%s"\n' "$DB_HOST"
		# shellcheck disable=SC2031
		printf 'db_name="%s"\n' "$DB_NAME"
		# shellcheck disable=SC2031
		printf 'db_user="%s"\n' "$DB_USER"
		# shellcheck disable=SC2031
		printf 'db_password="%s"\n' "$DB_PASS"
		return 1
	fi
	export PRESTASHOP_PASSWORD
	export PRESTASHOP_EMAIL
	return 0
}
prestashop_post_installation_cleanup() {
	printf 'Nettoyage post-configuration de PrestaShop…\n'
	check_dependencies 'git' 'mktemp' 'tr'
	check_variables 'GIT_REPO_HOST' 'LOCAL_REPO_PATH' 'PROJECT_HOSTNAME' 'PRESTASHOP_ADMIN_PATH'
	git_get_repository_url
	check_variables 'GIT_REPO_URL'
	printf 'Récupération du dépôt distant.\n'
	git clone "$GIT_REPO_URL" "$LOCAL_REPO_PATH" >/dev/null 2>&1
	(
		cd "$LOCAL_REPO_PATH" || return 1
		git rm -r 'install' >/dev/null 2>&1
		git mv 'admin' "$PRESTASHOP_ADMIN_PATH"
		git commit --message='Post installation clean-up' >/dev/null 2>&1
		printf 'Mise-à-jour du dépôt distant.\n'
		GIT_REPO_NAME="prestashop_$PROJECT_NAME"
		export GIT_REPO_NAME
		git push origin master >/dev/null 2>/dev/null
		git_add_branch_restrictions
	)
	rm --force --recursive "$LOCAL_REPO_PATH"
	printf '\033[1;32mOK\033[0m\n'
	if [ "$WIKI_ENGINE" = 'none' ]; then
		printf '#####\n'
		printf '# Accès au back-office PrestaShop\n'
		printf '# URL : "%s"\n' "https://$PROJECT_HOSTNAME/$PRESTASHOP_ADMIN_PATH/"
		printf '#####\n'
	fi
	return 0
}

