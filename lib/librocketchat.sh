rocketchat_set_service() {
	check_variables 'ROCKETCHAT_API_SERVER'
	REAL_TIME_CHAT_API_URL_PREFIX="$ROCKETCHAT_API_SERVER/api/v1"
	export REAL_TIME_CHAT_API_URL_PREFIX
}
rocketchat_get_common_curl_options() {
	check_variables 'ROCKETCHAT_API_USERID' 'ROCKETCHAT_API_TOKEN'
	CURL_OPTIONS="$CURL_OPTIONS --request POST"
	# shellcheck disable=SC2089
	CURL_OPTIONS="$CURL_OPTIONS --header 'X-Auth-Token: $ROCKETCHAT_API_TOKEN'"
	CURL_OPTIONS="$CURL_OPTIONS --header 'X-User-Id: $ROCKETCHAT_API_USERID'"
	CURL_OPTIONS="$CURL_OPTIONS --header 'Content-Type: application/json'"
	# shellcheck disable=SC2090
	export CURL_OPTIONS
}
rocketchat_create_channel() {
	check_variables 'ROCKETCHAT_API_USERID' 'ROCKETCHAT_API_TOKEN' 'REAL_TIME_CHAT_API_URL_PREFIX'
	rocketchat_get_common_curl_options
	check_variables 'CURL_OPTIONS'
	curl_data='{'
	curl_data="$curl_data \"name\": \"$PROJECT_NAME\""
	curl_data="$curl_data }"
	curl_options="$CURL_OPTIONS --data '$curl_data'"
	curl_url="$REAL_TIME_CHAT_API_URL_PREFIX/channels.create"
	send_api_call "$curl_options" "$curl_url"
}

