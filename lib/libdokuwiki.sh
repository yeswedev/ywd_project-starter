dokuwiki_write_page() {
	check_variables 'PROJECT_NAME_PRETTY' 'HTTP_USER' 'HTTP_PASS' 'PROJECT_HOSTNAME' 'PROJECT_NAME' 'SERVER_USER' 'DB_HOST' 'DB_NAME' 'DB_USER' 'DB_PASS' 'GIT_REPO_HTTP_URL'
	case "$PROJECT_TECH" in
		('prestashop'*)
			check_variables 'PRESTASHOP_ADMIN_PATH' 'PRESTASHOP_EMAIL' 'PRESTASHOP_PASSWORD'
		;;
	esac
	# shellcheck disable=SC2039
	local http_scheme
	case "$PROJECT_TECH" in
		('prestashop1.6')
			http_scheme='http://'
		;;
		(*)
			http_scheme='https://'
		;;
	esac
	WIKI_PAGE=$(mktemp --tmpdir="${TMPDIR:-/tmp}" 'wiki_page.XXXXX')
	cat > "$WIKI_PAGE" <<- EOF
	====== $PROJECT_NAME_PRETTY ======

	===== Reprise =====

	<code>
	cd ~/dev
	sudo project -d clone $GIT_REPO_URL
	</code>

	===== Serveurs =====

	==== Développement ====

	=== Accès au site ===

	[[${http_scheme}$HTTP_USER:$HTTP_PASS@$PROJECT_HOSTNAME/|${http_scheme}$PROJECT_HOSTNAME/]]

	== Authentification HTTP ==

	Désactiver :
	<code>
	ssh root@$PROJECT_HOSTNAME http-authentication_${PROJECT_NAME} disable
	</code>

	Activer :
	<code>
	ssh root@$PROJECT_HOSTNAME http-authentication_${PROJECT_NAME} enable
	</code>

	EOF
	case "$PROJECT_TECH" in
		('prestashop'*)
			cat >> "$WIKI_PAGE" <<- EOF
			== Administration ==

			[[${http_scheme}$HTTP_USER:$HTTP_PASS@$PROJECT_HOSTNAME/$PRESTASHOP_ADMIN_PATH/|${http_scheme}$PROJECT_HOSTNAME/$PRESTASHOP_ADMIN_PATH/]]

			  * $PRESTASHOP_EMAIL : ''$PRESTASHOP_EMAIL''
			  * $PRESTASHOP_PASSWORD : ''$PRESTASHOP_PASSWORD''

			EOF
		;;
	esac
	cat >> "$WIKI_PAGE" <<- EOF
	=== Déploiement auto ===

	<code>
	ssh $SERVER_USER@$PROJECT_HOSTNAME deploy_${PROJECT_NAME}
	</code>

	=== Base de données ===

  * DB_HOST : $DB_HOST
  * DB_NAME : $DB_NAME
  * DB_USER : $DB_USER
  * DB_PASS : $DB_PASS
	EOF
	export WIKI_PAGE
}
dokuwiki_send_page() {
	check_dependencies 'rsync'
	check_variables 'WIKI_PAGE' 'DOKUWIKI_SERVER_PORT' 'DOKUWIKI_SERVER_USER' 'DOKUWIKI_SERVER_HOST' 'DOKUWIKI_PAGES_PATH' 'PROJECT_NAME'
	chmod g+rw "$WIKI_PAGE"
	rsync -e "ssh -p $DOKUWIKI_SERVER_PORT" --perms "$WIKI_PAGE" "$DOKUWIKI_SERVER_USER@$DOKUWIKI_SERVER_HOST:$DOKUWIKI_PAGES_PATH/${PROJECT_NAME}.txt"
	rm "$WIKI_PAGE"
}
dokuwiki_display_url() {
	check_variables 'DOKUWIKI_INDEX_URL' 'PROJECT_NAME'
	# shellcheck disable=SC2039
	local page_url
	page_url="${DOKUWIKI_INDEX_URL}${PROJECT_NAME}"
	printf '#####\n'
	printf '# %s\n' 'URL de la page du projet sur lʼinstance DokuWiki :'
	printf '# %s\n' "$page_url"
	printf '#####\n'
	return 0
}
