gitlab_get_common_curl_options() {
	check_variables 'GITLAB_ACCESS_TOKEN'
	unset CURL_OPTIONS
	# shellcheck disable=SC2089
	CURL_OPTIONS="$CURL_OPTIONS --header 'PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN'"
	CURL_OPTIONS="$CURL_OPTIONS --header 'Content-Type: application/json'"
	# shellcheck disable=SC2090
	export CURL_OPTIONS
	return 0
}
gitlab_create_repository() {
	check_dependencies 'jq'
	check_variables 'GITLAB_BASE_URL' 'GITLAB_ACCESS_TOKEN' 'GITLAB_NAMESPACE_ID' 'GIT_REPO_NAME' 'PROJECT_NAME_PRETTY'
	gitlab_get_common_curl_options
	check_variables 'CURL_OPTIONS'
	curl_data='{'
	curl_data="$curl_data \"name\": \"$PROJECT_NAME_PRETTY\","
	curl_data="$curl_data \"path\": \"$GIT_REPO_NAME\","
	curl_data="$curl_data \"namespace_id\": \"$GITLAB_NAMESPACE_ID\""
	curl_data="$curl_data }"
	curl_options="$CURL_OPTIONS --request POST --data '$curl_data'"
	curl_url="$GITLAB_BASE_URL/projects"
	send_api_call "$curl_options" "$curl_url"
	GIT_REPO_ID=$(printf '%s' "$API_FEEDBACK" | jq --raw-output '.id')
	GIT_REPO_HTTP_URL=$(printf '%s' "$API_FEEDBACK" | jq --raw-output '.http_url_to_repo')
	GIT_REPO_URL=$(printf '%s' "$API_FEEDBACK" | jq --raw-output '.ssh_url_to_repo')
	export GIT_REPO_ID GIT_REPO_HTTP_URL GIT_REPO_URL
	if [ "$WIKI_ENGINE" = 'none' ]; then
		printf '#####\n'
		printf '# Accès au dépôt git\n'
		printf '# URL : %s\n' "$GIT_REPO_HTTP_URL"
		printf '# Clonage : git clone %s\n' "$GIT_REPO_URL"
		printf '#####\n'
	fi
	return 0
}
gitlab_add_ssh_deploy_key() {
	check_variables 'GITLAB_BASE_URL' 'GIT_REPO_ID' 'GITLAB_SSH_KEY_ID'
	gitlab_get_common_curl_options
	check_variables 'CURL_OPTIONS'
	curl_options="$CURL_OPTIONS --request POST"
	curl_url="$GITLAB_BASE_URL/projects/$GIT_REPO_ID/deploy_keys/$GITLAB_SSH_KEY_ID/enable"
	send_api_call "$curl_options" "$curl_url"
	return 0
}
gitlab_add_branch_restrictions() {
	check_variables 'GITLAB_BASE_URL' 'GITLAB_ACCESS_TOKEN' 'GIT_REPO_ID'
	gitlab_get_common_curl_options
	check_variables 'CURL_OPTIONS'
	curl_data='{'
	curl_data="$curl_data \"name\": \"master\","
	curl_data="$curl_data \"push_access_level\": \"0\","
	curl_data="$curl_data \"merge_access_level\": \"40\""
	curl_data="$curl_data }"
	curl_options="$CURL_OPTIONS --data '$curl_data'"
	curl_url="$GITLAB_BASE_URL/projects/$GIT_REPO_ID/protected_branches"
	send_api_call "$curl_options" "$curl_url"
	curl_data='{'
	curl_data="$curl_data \"name\": \"staging\","
	curl_data="$curl_data \"push_access_level\": \"0\","
	curl_data="$curl_data \"merge_access_level\": \"40\""
	curl_data="$curl_data }"
	curl_options="$CURL_OPTIONS --data '$curl_data'"
	curl_url="$GITLAB_BASE_URL/projects/$GIT_REPO_ID/protected_branches"
	send_api_call "$curl_options" "$curl_url"
	curl_data='{'
	curl_data="$curl_data \"name\": \"dev\","
	curl_data="$curl_data \"push_access_level\": \"40\","
	curl_data="$curl_data \"merge_access_level\": \"40\""
	curl_data="$curl_data }"
	curl_options="$CURL_OPTIONS --data '$curl_data'"
	curl_url="$GITLAB_BASE_URL/projects/$GIT_REPO_ID/protected_branches"
	send_api_call "$curl_options" "$curl_url"
	return 0
}

