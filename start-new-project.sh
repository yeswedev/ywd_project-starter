#!/bin/sh
set -e

SCRIPT_PATH="$(dirname "$(readlink --canonicalize "$0")")"

( cd "$SCRIPT_PATH" && make )
# shellcheck source=libtoolbox.sh
. "$SCRIPT_PATH/libtoolbox.sh"

parse_configuration
set_project_infos
parse_configuration

# Create real time chat channel
real_time_chat_create_channel

# Create git repository
git_get_repository_name
git_set_repository_host
git_create_repository
git_get_local_repository_path
git_initiate_project
git_link_to_distant_repository
rm --force --recursive "$LOCAL_REPO_PATH"
git_add_ssh_deploy_key
git_add_branch_restrictions

# Create project on distant server
check_dependencies 'rsync' 'ssh' 'mktemp'
: "${SERVER_SSH_PORT:='22'}"
if [ "$DEBUG" = 'true' ] && [ "$DEBUG_GENERIC" = 'true' ]; then
	printf 'Using SSH port: %s\n' "$SERVER_SSH_PORT"
fi
# shellcheck disable=SC2086
ssh -p $SERVER_SSH_PORT root@$SERVER_HOSTNAME 'if [ -e /root/ywd_project-starter.git ]; then ( cd /root/ywd_project-starter.git && git pull ); else git clone https://framagit.org/yeswedev/ywd_project-starter.git /root/ywd_project-starter.git; fi'
# shellcheck disable=SC2086
rsync -e "ssh -p $SERVER_SSH_PORT" "$SCRIPT_PATH"/config/*.conf "$SCRIPT_PATH"/config/*.info root@$SERVER_HOSTNAME:/root/ywd_project-starter.git/config/
# shellcheck disable=SC2086
ssh -p $SERVER_SSH_PORT root@$SERVER_HOSTNAME /root/ywd_project-starter.git/actions/create-nginx-website.sh
# shellcheck disable=SC2086
ssh -p $SERVER_SSH_PORT root@$SERVER_HOSTNAME /root/ywd_project-starter.git/actions/create-database.sh
# shellcheck disable=SC2086
ssh -p $SERVER_SSH_PORT root@$SERVER_HOSTNAME /root/ywd_project-starter.git/actions/set-up-toolbox.sh
case "$PROJECT_TECH" in
	('prestashop'*)
		LOCAL_REPO_PATH="$(mktemp --directory --tmpdir="${TMPDIR:-/tmp}" "$PROJECT_NAME.XXXXX")"
		export LOCAL_REPO_PATH
		prestashop_post_installation_cleanup
	;;
esac
ssh-keyscan -v -t rsa "$PROJECT_HOSTNAME" >> ~/.ssh/known_hosts 2>/dev/null
# shellcheck disable=SC2086
ssh -p $SERVER_SSH_PORT root@$SERVER_HOSTNAME /root/ywd_project-starter.git/actions/write-new-wiki-page.sh

exit 0
