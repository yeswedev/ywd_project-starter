#!/bin/sh
set -e

SCRIPT_PATH="$(dirname "$(readlink --canonicalize "$0")")/.."

( cd "$SCRIPT_PATH" && make )
# shellcheck source=libtoolbox.sh
. "$SCRIPT_PATH/libtoolbox.sh"

if [ "$DEBUG" = 'true' ] && [ "$DEBUG_GENERIC" = 'true' ]; then
	printf 'Entering create-database.sh\n'
fi

parse_configuration

server_database_create
case "$PROJECT_TECH" in
	('laravel')
		laravel_database_link
	;;
	('wordpress'*)
		wordpress_database_link
	;;
	('prestashop'*)
		prestashop_database_link
	;;
	('drupal')
		drupal_database_link
	;;
esac

if [ "$DEBUG" = 'true' ] && [ "$DEBUG_GENERIC" = 'true' ]; then
	printf 'Leaving create-database.sh\n'
fi

exit 0
