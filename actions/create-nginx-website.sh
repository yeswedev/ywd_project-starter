#!/bin/sh
set -e

SCRIPT_PATH="$(dirname "$(readlink --canonicalize "$0")")/.."

( cd "$SCRIPT_PATH" && make )
# shellcheck source=libtoolbox.sh
. "$SCRIPT_PATH/libtoolbox.sh"

if [ "$DEBUG" = 'true' ] && [ "$DEBUG_GENERIC" = 'true' ]; then
	printf 'Entering create-nginx-website.sh\n'
fi

parse_configuration

git_set_repository_host
nginx_create_website_configuration
nginx_enable_https_with_certbot
git_clone_repository_on_server

if [ "$DEBUG" = 'true' ] && [ "$DEBUG_GENERIC" = 'true' ]; then
	printf 'Leaving create-nginx-website.sh\n'
fi

exit 0
