#!/bin/sh
set -e

SCRIPT_PATH="$(dirname "$(readlink --canonicalize "$0")")/.."

( cd "$SCRIPT_PATH" && make )
# shellcheck source=libtoolbox.sh
. "$SCRIPT_PATH/libtoolbox.sh"

if [ "$DEBUG" = 'true' ] && [ "$DEBUG_GENERIC" = 'true' ]; then
	printf 'Entering set-up-toolbox.sh\n'
fi

parse_configuration

server_set_up_toolbox

if [ "$DEBUG" = 'true' ] && [ "$DEBUG_GENERIC" = 'true' ]; then
	printf 'Leaving set-up-toolbox.sh\n'
fi

exit 0
