#!/bin/sh
set -e

SCRIPT_PATH="$(dirname "$(readlink --canonicalize "$0")")/.."

( cd "$SCRIPT_PATH" && make )
# shellcheck source=libtoolbox.sh
. "$SCRIPT_PATH/libtoolbox.sh"

if [ "$DEBUG" = 'true' ] && [ "$DEBUG_GENERIC" = 'true' ]; then
	printf 'Entering wiki-new-page.sh\n'
fi

parse_configuration

wiki_write_page
wiki_send_page
wiki_display_url

if [ "$DEBUG" = 'true' ] && [ "$DEBUG_GENERIC" = 'true' ]; then
	printf 'Leaving wiki-new-page.sh\n'
fi

exit 0
