all: libtoolbox.sh

libtoolbox.sh: lib/*
	@cat lib/* > libtoolbox.sh

clean:
	rm -f libtoolbox.sh
